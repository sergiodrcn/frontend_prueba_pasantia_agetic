import Head from 'next/head'
import styles from '../styles/Home.module.css'
import { fetchApiData } from '../api/api'
import { useEffect, useState } from 'react';


const about = ({ resultado }) => {
  console.log(resultado)
  const [nombre, setNombre] = useState('');
  const [email, setEmail] = useState('');
  const [telefono, setTelefono] = useState('');

  const handleSubmit = (event) => {
    event.preventDefault();
    // Aquí puedes hacer lo que necesites con los datos del formulario
  };
  var bandera = 0;
  return (
    <div className={styles.container}>
      <Head>
        <title>Prueba Pasante</title>

      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Prueba Pasante Agetic
        </h1>
        <br></br>
        <p className={styles.description}>
          Sergio Ramirez Castro    Cel. 77589972    ramirezinformation@gmail.com
        </p>

        {/* <div className={styles.grid}>
          <a href="#" className={styles.card}>
            <h3>Listar</h3>
            <p>Some text for Card 1.</p>
          </a>

          <a href="#" className={styles.card}>
            <h3>Crear</h3>
            <p>Some text for Card 2.</p>
          </a>

          <a href="#" className={styles.card}>
            <h3>Editar</h3>
            <p>Some text for Card 3.</p>
          </a>

          <a href="#" className={styles.card}>
            <h3>Eliminar</h3>
            <p>Some text for Card 3.</p>
          </a>
        </div> */}
      </main>
      <div id='form' className={styles.container}>
        <div>
          <label className={styles.clabel2}>
            PRODUCTOS
          </label>
          <label className={styles.clabel}>
            Nombre:
            <input id='cajaNombre' className={styles.cinput} type="text" />
          </label>
          <label>
            Cantidad:
            <input id='cajaCantidad' className={styles.cinput} type="number" />
          </label>
          <label>
            Precio:
            <input id='cajaPrecio' className={styles.cinput} type="number" value={telefono} onChange={(e) => setTelefono(e.target.value)} />
          </label>
          <button id='buttonAgregar' className={styles.boton} onClick={() => {
            const url = 'http://localhost:3000/productos';
            const reserva = {
              nombre: document.getElementById("cajaNombre").value,
              cantidad: 0 + document.getElementById("cajaCantidad").value,
              precioReferencial: 0 + document.getElementById("cajaPrecio").value,
              fechaRegistro: new Date().toJSON(),
            };

            const jsonData = JSON.stringify(reserva);
            console.log(jsonData);
            fetch(url, {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json'
              },
              body: jsonData
            }).then(data => window.location.reload());
          }}
          >Agregar</button>
          <button id='buttonActualizar' style={{ visibility: 'hidden' }} className={styles.boton} onClick={() => {
            const url = 'http://localhost:3000/productos/' + bandera;
            const reserva = {
              nombre: document.getElementById("cajaNombre").value,
              cantidad: 0 + document.getElementById("cajaCantidad").value,
              precioReferencial: 0 + document.getElementById("cajaPrecio").value,
              fechaActualizacion: new Date().toJSON(),
            };
            console.log(reserva);
            console.log(bandera)
            const jsonData = JSON.stringify(reserva);
            console.log(jsonData);
            fetch(url, {
              method: 'PUT',
              headers: {
                'Content-Type': 'application/json'
              },
              body: jsonData
            }).then(data => window.location.reload());
          }}
          >Actualizar</button>
          <button id='buttonCancelar' style={{ visibility: 'hidden' }} className={styles.boton2} onClick={() => {
            window.location.reload();
          }}
          >Cancelar</button>
        </div>
        <div>
          <h1>API Data Table</h1>

          <table className={styles.tabla}>
            <thead>
              <tr>
                <th>id</th>
                <th>nombre</th>
                <th>cantidad</th>
                <th>precioReferencial</th>
                <th>fechaRegistro</th>
                <th>fechaActualizacion</th>
                <th>Editar</th>
                <th>Eliminar</th>
              </tr>
            </thead>
            <tbody>
              {resultado.map((dato) => (
                <tr key={dato.id}>
                  <td>{dato.id}</td>
                  <td>{dato.nombre}</td>
                  <td>{dato.cantidad}</td>
                  <td>{dato.precioReferencial}</td>
                  <td>{dato.fechaRegistro}</td>
                  <td>{dato.fechaActualizacion}</td>
                  <td style={{ textAlign: 'center' }}>
                    <button type="button" onClick={() => {
                      const url = 'http://localhost:3000/productos/' + dato.id;
                      fetch(url, { method: 'DELETE' })
                        .then((response) => {
                          if (!response.ok) {
                            throw new Error('que raro')
                          }
                          return response.json()
                        }
                        )
                        .then((data) => { window.location.reload(); })
                        .catch((e) => {
                          console.log(e);
                        })
                    }} style={{ backgroundColor: 'white' }}>
                      ❌
                    </button>
                  </td>
                  <td style={{ textAlign: 'center' }}>
                    <button type="button" onClick={() => {
                      // llenar cajas
                      bandera = dato.id;
                      document.getElementById("cajaNombre").value = dato.nombre;
                      document.getElementById("cajaCantidad").value = dato.cantidad;
                      document.getElementById("cajaPrecio").value = dato.precioReferencial;
                      document.getElementById("buttonAgregar").style.visibility = 'hidden';
                      document.getElementById("buttonActualizar").style.visibility = '';
                      document.getElementById("buttonCancelar").style.visibility = '';
                      const elemento = document.getElementById('form');
                      elemento.scrollIntoView({ behavior: 'smooth' });
                      //fin llenado
                      // const url = 'http://localhost:3000/productos/' + dato.id;
                      // const reserva = {
                      //   nombre: document.getElementById("cajaNombre").value,
                      //   cantidad: 0+document.getElementById("cajaCantidad").value,
                      //   precioReferencial: 0+document.getElementById("cajaPrecio").value,
                      //   fechaRegistro:new Date().toJSON(),
                      // };
                      // console.log(reserva);
                      // const jsonData = JSON.stringify(reserva);
                      // console.log(jsonData);
                      // fetch(url, {
                      //   method: 'POST',
                      //   headers: {
                      //     'Content-Type': 'application/json'
                      //   },
                      //   body: jsonData
                      // }).then(data => window.location.reload());
                    }} style={{ backgroundColor: 'green' }}>
                      ✎
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>

  )
}
async function eliminar(idx) {
  console.log("Holaaa");
  const res2 = await fetch(`http://localhost:3000/productos/${idx}`, {
    method: 'DELETE'
  });
  return res2.json();
  // deleteData(idx)
}
function editar(idx) {
  console.log(idx);
}
export async function getServerSideProps() {
  const url = 'http://localhost:3000/productos'
  const respuesta = await fetch(url)
  const resultado = await respuesta.json()

  return {
    props: {
      resultado
    }
  }
}
// async function deleteData(id) {
//   const res = await fetch(`http://localhost:3000/productos/${id}`, {
//     method: 'DELETE'
//   });
//   return res.json();
// }
export default about