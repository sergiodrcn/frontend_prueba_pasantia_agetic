// import Image from 'next/image'
// import { Inter } from 'next/font/google'

// const inter = Inter({ subsets: ['latin'] })

// export default function Home() {
//   return (
//     <main
//       className={`flex min-h-screen flex-col items-center justify-between p-24 ${inter.className}`}
//     >
//       <div className="z-10 w-full max-w-5xl items-center justify-between font-mono text-sm lg:flex">
//         <p className="fixed left-0 top-0 flex w-full justify-center border-b border-gray-300 bg-gradient-to-b from-zinc-200 pb-6 pt-8 backdrop-blur-2xl dark:border-neutral-800 dark:bg-zinc-800/30 dark:from-inherit lg:static lg:w-auto  lg:rounded-xl lg:border lg:bg-gray-200 lg:p-4 lg:dark:bg-zinc-800/30">
//           Get started by editing&nbsp;
//           <code className="font-mono font-bold">src/pages/index.tsx</code>
//         </p>
//         <div className="fixed bottom-0 left-0 flex h-48 w-full items-end justify-center bg-gradient-to-t from-white via-white dark:from-black dark:via-black lg:static lg:h-auto lg:w-auto lg:bg-none">
//           <a
//             className="pointer-events-none flex place-items-center gap-2 p-8 lg:pointer-events-auto lg:p-0"
//             href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template-tw&utm_campaign=create-next-app"
//             target="_blank"
//             rel="noopener noreferrer"
//           >
//             By{' '}
//             <Image
//               src="/vercel.svg"
//               alt="Vercel Logo"
//               className="dark:invert"
//               width={100}
//               height={24}
//               priority
//             />
//           </a>
//         </div>
//       </div>

//       <div className="relative flex place-items-center before:absolute before:h-[300px] before:w-[480px] before:-translate-x-1/2 before:rounded-full before:bg-gradient-radial before:from-white before:to-transparent before:blur-2xl before:content-[''] after:absolute after:-z-20 after:h-[180px] after:w-[240px] after:translate-x-1/3 after:bg-gradient-conic after:from-sky-200 after:via-blue-200 after:blur-2xl after:content-[''] before:dark:bg-gradient-to-br before:dark:from-transparent before:dark:to-blue-700/10 after:dark:from-sky-900 after:dark:via-[#0141ff]/40 before:lg:h-[360px]">
//         <Image
//           className="relative dark:drop-shadow-[0_0_0.3rem_#ffffff70] dark:invert"
//           src="/next.svg"
//           alt="Next.js Logo"
//           width={180}
//           height={37}
//           priority
//         />
//       </div>

//       <div className="mb-32 grid text-center lg:mb-0 lg:grid-cols-4 lg:text-left">
//         <a
//           href="https://nextjs.org/docs?utm_source=create-next-app&utm_medium=default-template-tw&utm_campaign=create-next-app"
//           className="group rounded-lg border border-transparent px-5 py-4 transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           <h2 className={`mb-3 text-2xl font-semibold`}>
//             Docs{' '}
//             <span className="inline-block transition-transform group-hover:translate-x-1 motion-reduce:transform-none">
//               -&gt;
//             </span>
//           </h2>
//           <p className={`m-0 max-w-[30ch] text-sm opacity-50`}>
//             Find in-depth information about Next.js features and API.
//           </p>
//         </a>

//         <a
//           href="https://nextjs.org/learn?utm_source=create-next-app&utm_medium=default-template-tw&utm_campaign=create-next-app"
//           className="group rounded-lg border border-transparent px-5 py-4 transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           <h2 className={`mb-3 text-2xl font-semibold`}>
//             Learn{' '}
//             <span className="inline-block transition-transform group-hover:translate-x-1 motion-reduce:transform-none">
//               -&gt;
//             </span>
//           </h2>
//           <p className={`m-0 max-w-[30ch] text-sm opacity-50`}>
//             Learn about Next.js in an interactive course with&nbsp;quizzes!
//           </p>
//         </a>

//         <a
//           href="https://vercel.com/templates?framework=next.js&utm_source=create-next-app&utm_medium=default-template-tw&utm_campaign=create-next-app"
//           className="group rounded-lg border border-transparent px-5 py-4 transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           <h2 className={`mb-3 text-2xl font-semibold`}>
//             Templates{' '}
//             <span className="inline-block transition-transform group-hover:translate-x-1 motion-reduce:transform-none">
//               -&gt;
//             </span>
//           </h2>
//           <p className={`m-0 max-w-[30ch] text-sm opacity-50`}>
//             Discover and deploy boilerplate example Next.js&nbsp;projects.
//           </p>
//         </a>

//         <a
//           href="https://vercel.com/new?utm_source=create-next-app&utm_medium=default-template-tw&utm_campaign=create-next-app"
//           className="group rounded-lg border border-transparent px-5 py-4 transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           <h2 className={`mb-3 text-2xl font-semibold`}>
//             Deploy{' '}
//             <span className="inline-block transition-transform group-hover:translate-x-1 motion-reduce:transform-none">
//               -&gt;
//             </span>
//           </h2>
//           <p className={`m-0 max-w-[30ch] text-sm opacity-50`}>
//             Instantly deploy your Next.js site to a shareable URL with Vercel.
//           </p>
//         </a>
//       </div>
//     </main>
//   )
// }

import Head from 'next/head'
import styles from '../styles/Home.module.css'
import { fetchApiData } from '../api/api'
import { useEffect, useState } from 'react';


const about = ({ resultado }) => {
  console.log(resultado)
  const [nombre, setNombre] = useState('');
  const [email, setEmail] = useState('');
  const [telefono, setTelefono] = useState('');

  const handleSubmit = (event) => {
    event.preventDefault();
    // Aquí puedes hacer lo que necesites con los datos del formulario
  };
  var bandera = 0;
  return (
    <div className={styles.container}>
      <Head>
        <title>Prueba Pasante</title>

      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Prueba Pasante Agetic
        </h1>
        <br></br>
        <p className={styles.description}>
          Sergio Ramirez Castro    Cel. 77589972    ramirezinformation@gmail.com
        </p>

        {/* <div className={styles.grid}>
          <a href="#" className={styles.card}>
            <h3>Listar</h3>
            <p>Some text for Card 1.</p>
          </a>

          <a href="#" className={styles.card}>
            <h3>Crear</h3>
            <p>Some text for Card 2.</p>
          </a>

          <a href="#" className={styles.card}>
            <h3>Editar</h3>
            <p>Some text for Card 3.</p>
          </a>

          <a href="#" className={styles.card}>
            <h3>Eliminar</h3>
            <p>Some text for Card 3.</p>
          </a>
        </div> */}
      </main>
      <div id='form' className={styles.container}>
        <div>
          <label className={styles.clabel2}>
            PRODUCTOS
          </label>
          <label className={styles.clabel}>
            Nombre:
            <input id='cajaNombre' className={styles.cinput} type="text" />
          </label>
          <label>
            Cantidad:
            <input id='cajaCantidad' className={styles.cinput} type="number" />
          </label>
          <label>
            Precio:
            <input id='cajaPrecio' className={styles.cinput} type="number" value={telefono} onChange={(e) => setTelefono(e.target.value)} />
          </label>
          <button id='buttonAgregar' className={styles.boton} onClick={() => {
            const url = 'http://localhost:3000/productos';
            const reserva = {
              nombre: document.getElementById("cajaNombre").value,
              cantidad: 0 + document.getElementById("cajaCantidad").value,
              precioReferencial: 0 + document.getElementById("cajaPrecio").value,
              fechaRegistro: new Date().toJSON(),
            };

            const jsonData = JSON.stringify(reserva);
            console.log(jsonData);
            fetch(url, {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json'
              },
              body: jsonData
            }).then(data => window.location.reload());
          }}
          >Agregar</button>
          <button id='buttonActualizar' style={{ visibility: 'hidden' }} className={styles.boton} onClick={() => {
            const url = 'http://localhost:3000/productos/' + bandera;
            const reserva = {
              nombre: document.getElementById("cajaNombre").value,
              cantidad: 0 + document.getElementById("cajaCantidad").value,
              precioReferencial: 0 + document.getElementById("cajaPrecio").value,
              fechaActualizacion: new Date().toJSON(),
            };
            console.log(reserva);
            console.log(bandera)
            const jsonData = JSON.stringify(reserva);
            console.log(jsonData);
            fetch(url, {
              method: 'PUT',
              headers: {
                'Content-Type': 'application/json'
              },
              body: jsonData
            }).then(data => window.location.reload());
          }}
          >Actualizar</button>
          <button id='buttonCancelar' style={{ visibility: 'hidden' }} className={styles.boton2} onClick={() => {
            window.location.reload();
          }}
          >Cancelar</button>
        </div>
        <div>
          <h1>API Data Table</h1>

          <table className={styles.tabla}>
            <thead>
              <tr>
                <th>id</th>
                <th>nombre</th>
                <th>cantidad</th>
                <th>precioReferencial</th>
                <th>fechaRegistro</th>
                <th>fechaActualizacion</th>
                <th>Editar</th>
                <th>Eliminar</th>
              </tr>
            </thead>
            <tbody>
              {resultado.map((dato) => (
                <tr key={dato.id}>
                  <td>{dato.id}</td>
                  <td>{dato.nombre}</td>
                  <td>{dato.cantidad}</td>
                  <td>{dato.precioReferencial}</td>
                  <td>{dato.fechaRegistro}</td>
                  <td>{dato.fechaActualizacion}</td>
                  <td style={{ textAlign: 'center' }}>
                    <button type="button" onClick={() => {
                      const url = 'http://localhost:3000/productos/' + dato.id;
                      fetch(url, { method: 'DELETE' })
                        .then((response) => {
                          if (!response.ok) {
                            throw new Error('que raro')
                          }
                          return response.json()
                        }
                        )
                        .then((data) => { window.location.reload(); })
                        .catch((e) => {
                          console.log(e);
                        })
                    }} style={{ backgroundColor: 'white' }}>
                      ❌
                    </button>
                  </td>
                  <td style={{ textAlign: 'center' }}>
                    <button type="button" onClick={() => {
                      // llenar cajas
                      bandera = dato.id;
                      document.getElementById("cajaNombre").value = dato.nombre;
                      document.getElementById("cajaCantidad").value = dato.cantidad;
                      document.getElementById("cajaPrecio").value = dato.precioReferencial;
                      document.getElementById("buttonAgregar").style.visibility = 'hidden';
                      document.getElementById("buttonActualizar").style.visibility = '';
                      document.getElementById("buttonCancelar").style.visibility = '';
                      const elemento = document.getElementById('form');
                      elemento.scrollIntoView({ behavior: 'smooth' });
                      //fin llenado
                      // const url = 'http://localhost:3000/productos/' + dato.id;
                      // const reserva = {
                      //   nombre: document.getElementById("cajaNombre").value,
                      //   cantidad: 0+document.getElementById("cajaCantidad").value,
                      //   precioReferencial: 0+document.getElementById("cajaPrecio").value,
                      //   fechaRegistro:new Date().toJSON(),
                      // };
                      // console.log(reserva);
                      // const jsonData = JSON.stringify(reserva);
                      // console.log(jsonData);
                      // fetch(url, {
                      //   method: 'POST',
                      //   headers: {
                      //     'Content-Type': 'application/json'
                      //   },
                      //   body: jsonData
                      // }).then(data => window.location.reload());
                    }} style={{ backgroundColor: 'green' }}>
                      ✎
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>

  )
}
async function eliminar(idx) {
  console.log("Holaaa");
  const res2 = await fetch(`http://localhost:3000/productos/${idx}`, {
    method: 'DELETE'
  });
  return res2.json();
  // deleteData(idx)
}
function editar(idx) {
  console.log(idx);
}
export async function getServerSideProps() {
  const url = 'http://localhost:3000/productos'
  const respuesta = await fetch(url)
  const resultado = await respuesta.json()

  return {
    props: {
      resultado
    }
  }
}
// async function deleteData(id) {
//   const res = await fetch(`http://localhost:3000/productos/${id}`, {
//     method: 'DELETE'
//   });
//   return res.json();
// }
export default about