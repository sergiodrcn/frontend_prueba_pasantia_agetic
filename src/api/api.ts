export async function fetchApiData(): Promise<any> {
    const response = await fetch('http://localhost:3000/productos');
    const data = await response.json();
    return data;
  }
  